<?php namespace KEERill\Transactions;

use Backend;
use System\Classes\PluginBase;

/**
 * Transactions Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Transactions',
            'description' => 'No description provided yet...',
            'author'      => 'kEERill',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'KEERill\Transactions\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'keerill.transactions.some_permission' => [
                'tab' => 'Transactions',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'transactions' => [
                'label'       => 'Transactions',
                'url'         => Backend::url('keerill/transactions/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['keerill.transactions.*'],
                'order'       => 500,
            ],
        ];
    }
}
